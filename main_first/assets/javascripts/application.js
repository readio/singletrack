	(function($){
	  tabs = function() {
	    $('.tab-nav li:first').addClass('active');
	    $('.tab-container .tab-content:not(:first)').hide();
	    $('#tabs li a').click(function(){
	      	var t = $(this).attr('href');
	        $('.tab-nav li').removeClass('active');           
	        $(this).parent().addClass('active');
	        $(this).closest('.tab-nav').next('.tab-container').find('.tab-content').hide(); 
	        if (t == '#taball') {
	        	 $(this).closest('.tab-nav').next('.tab-container').find('.tab-content').fadeIn('slow');
	        } else {
	        	$(t).fadeIn('slow');
	        }
	        return false;
	    });
	  };
	})(jQuery);
	
	
	(function($){
	  gallery = function() {
			$('.gallery-link').on('click', function () {
			    $(this).next().magnificPopup('open');
			});
		
			$('.gallery').each(function () {
			    $(this).magnificPopup({
			        delegate: 'a',
			        type: 'image',
			        gallery: {
			            enabled: true,
			            navigateByImgClick: true
			        },
			        fixedContentPos: false,
			        mainClass: 'mfp-fade'
			    });
			});
		};
	})(jQuery);
	 
	(function($){
	  more = function() {
		$('.more-link').next('.more-content').hide();
		$('.more-link').click(function() {
			$(this).next('.more-content').slideToggle();
			$(this).toggleClass('active');
			$(this).find('span').toggleClass('icon-arrow-down');
			$(this).find('span').toggleClass('icon-arrow-up');
		});
		$('.more-show').on('click', function() {
			$(this).closest('.more-list').find('.more-content').slideDown();
			$(this).closest('.more-list').find('.more-link span').removeClass('icon-arrow-down');
			$(this).closest('.more-list').find('.more-link span').addClass('icon-arrow-up');
			return false;
		});
		$('.more-hide').on('click', function() {
			$(this).closest('.more-list').find('.more-content').slideUp();
			$(this).closest('.more-list').find('.more-link span').removeClass('icon-arrow-up');
			$(this).closest('.more-list').find('.more-link span').addClass('icon-arrow-down');
			return false;
		});
	  };
	})(jQuery);
	
	$(function() {
	  scrollTo = function() {
		  $('a.scroll').click(function() {
		  	var topHeight = $('.conclusion-banner').height();
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top - (topHeight + 20)
		        }, 1000);
		        return false;
		      }
		    }
		  });
	  }
	});
	
	
	$(function() {
		var seriesOptions = [],
			yAxisOptions = [],
			seriesCounter = 0,
			names = ['MSFT', 'AAPL', 'GOOG'],
			colors = Highcharts.getOptions().colors;
	
		$.each(names, function(i, name) {
	
			$.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename='+ name.toLowerCase() +'-c.json&callback=?',	function(data) {
	
				seriesOptions[i] = {
					name: name,
					data: data
				};
	
				// As we're loading the data asynchronously, we don't know what order it will arrive. So
				// we keep a counter and create the chart when all the data is loaded.
				seriesCounter++;
	
				if (seriesCounter == names.length) {
					createChart();
				}
			});
		});
	
	
	
		// create the chart when all data is loaded
		function createChart() {
	
			$('#chart-container').highcharts('StockChart', {
			    chart: {
			    },
	
			    rangeSelector: {
			        selected: 4
			    },
	
			    yAxis: {
			    	labels: {
			    		formatter: function() {
			    			return (this.value > 0 ? '+' : '') + this.value + '%';
			    		}
			    	},
			    	plotLines: [{
			    		value: 0,
			    		width: 2,
			    		color: 'silver'
			    	}]
			    },
			    
			    plotOptions: {
			    	series: {
			    		compare: 'percent'
			    	}
			    },
			    
			    tooltip: {
			    	pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
			    	valueDecimals: 2
			    },
			    
			    series: seriesOptions
			});
		}
	
	});
		
	$(document).ready(function() {    
	 	tabs();
		more();
		scrollTo();
		gallery();
	});